const postsController = require('../controller/adminPostsController');
const restrict = require('../middlewares/restrict');
const router = require('express').Router();

router.use('/', [restrict]);
router.get('/create', postsController.show);
router.post('/', postsController.create);
router.get('/:id', postsController.show);
router.put('/:id', postsController.update);
router.delete('/:id', postsController.destroy);

module.exports = router;
