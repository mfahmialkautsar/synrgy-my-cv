const router = require('express').Router();
const restrict = require('../middlewares/restrict');
const auth = require('../controller/authController');
const postsController = require('../controller/adminPostsController')
const posts = require('./adminPosts')

router.post('/api/v1/auth/register', auth.register)
router.post('/api/v1/auth/login', auth.login)

router.get('/register', (req, res) => res.render('admin/register'))
router.get('/login', (req, res) => res.render('admin/login'))

router.post('/logout', (req, res) => {
  res.clearCookie('token')
  res.redirect('/')
})
router.get('/whoami', restrict, (req, res) => res.send(req.user))

router.get('/dashboard', restrict, postsController.index)
router.use('/posts', posts)

module.exports = router