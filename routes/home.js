const router = require('express').Router();

router.get('/', function (req, res) {
  res.render('home', { layout: 'layouts/index' });
});

module.exports = router;
