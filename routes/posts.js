const router = require('express').Router()
const postController = require('../controller/postsController')

router.get('/:id', postController.show)

module.exports = router