const express = require('express');
const router = express.Router();
const path = require('path');
const createError = require('http-errors');
const home = require('./home');
const auth = require('./auth');
const posts = require('./posts');
const postsController = require('../controller/postsController');

/* GET home page. */
router.use('/', home);
router.use('/admin', auth);

router.get('/blog', postsController.index);
router.use('/blog/posts', posts)

router.use(
  express.static(path.join(path.dirname(require.main.filename), '..', 'public'))
);

// catch 404 and forward to error handler
router.use(function (req, res, next) {
  next(createError(404));
});

// error handler
router.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err.message);
  res.render('404');
});

module.exports = router;
