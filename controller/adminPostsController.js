const { posts } = require('../models');

module.exports = {
  index: (req, res, next) => {
    posts
      .findAll()
      .then((posts) =>
        res.render('admin/posts', { layout: 'admin/layouts/index', posts })
      )
      .catch(next);
  },
  show: (req, res) => {
    const { id } = req.params;
    if (!id) return res.render('admin/posts/show', { layout: 'admin/layouts/index' });
    
    const query = { where: { id } };
    posts
      .findOne(query)
      .then((post) => {
        if (!post) throw Error();
        res.render('admin/posts/show', {
          layout: 'admin/layouts/index',
          post,
        });
      })
      .catch((err) => {
        res.status(400);
        res.send(err);
      });
  },
  create: (req, res) => {
    posts
      .create(req.body)
      .then(() => {
        res.status(201);
        res.send('/admin/dashboard');
      })
      .then((err) => {
        res.status(400);
        res.send(err);
      });
  },
  update: (req, res) => {
    const { id } = req.params;
    const query = { where: { id } };
    console.log('THISS', req.body);
    posts
      .update(req.body, query)
      .then(() => {
        res.send('/admin/dashboard');
      })
      .catch((err) => {
        res.status(400);
        res.send(err);
      });
  },
  destroy: (req, res) => {
    const { id } = req.params;
    const query = { where: { id } };
    posts
      .destroy(query)
      .then(() => res.send('/admin/dashboard'))
      .catch((err) => {
        res.status(400);
        res.send(err);
      });
  },
};
