const { users } = require('../models');

function format(user) {
  const { id, username } = user;
  return {
    id,
    username,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  register: (req, res, next) => {
    users.register(req.body)
      .then(() => {
        res.redirect('/admin/login');
      })
      .catch(next);
  },
  login: (req, res, next) => {
    users.authenticate(req.body)
    .then((user) => {
      const result = format(user)
      res.cookie('token', result.accessToken)
      res.redirect('/admin/dashboard')
    })
    .catch(err => res.redirect('/admin/login'));
  },
};
