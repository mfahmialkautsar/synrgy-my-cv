const {posts} = require('../models')

module.exports = {
  index: (req, res, next) => {
    posts
      .findAll()
      .then((posts) =>
        res.render('posts/index', { layout: 'layouts/blog', posts })
      )
      .catch(next);
  },
  show: (req, res) => {
    const { id } = req.params;
    const query = { where: { id } };
    posts
      .findOne(query)
      .then((post) => {
        if (!post) throw Error();
        res.render('posts/show', { layout: 'layouts/blog', post })
      })
      .catch((err) => {
        res.status(400);
        res.send(err);
      });
  },
};
