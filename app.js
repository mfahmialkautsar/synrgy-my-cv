require('dotenv').config();

const express = require('express');
const partials = require('express-partials');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('./lib/passport')

const router = require('./routes')

const app = express();

app.use(partials())

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.set('view engine', 'ejs');

app.use(passport.initialize())
app.use(router);

module.exports = app;
