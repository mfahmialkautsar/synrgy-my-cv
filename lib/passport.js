const passport = require('passport');
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const { users } = require('../models');

const options = {
  jwtFromRequest: ExtractJwt.fromExtractors([req => req.cookies.token]),
  secretOrKey: process.env.TOKEN_KEY,
};

passport.use(
  new JwtStrategy(options, async (payload, done) => {
    users.findByPk(payload.id)
      .then((user) => done(null, user))
      .catch((err) => done(err, false));
  })
);

module.exports = passport;
