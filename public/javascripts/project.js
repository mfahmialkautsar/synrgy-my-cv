const projects = (completion) =>
  $.getJSON(
    'https://raw.githubusercontent.com/mfahmialkautsar/mfahmialkautsar.github.io/master/src/data/projects.json',
    function (data) {
      completion(data);
    }
  );
const designs = (completion) =>
  $.getJSON(
    'https://raw.githubusercontent.com/mfahmialkautsar/mfahmialkautsar.github.io/master/src/data/designs.json',
    function (data) {
      completion(data);
    }
  );

function project() {
  const outsideSource = /^https:\/\/.+/;
  const imgSource = (img) =>
    img.match(outsideSource) ? img : 'https://mfahmialkautsar.github.io/' + img;
  function createProjectContainer({title, img, category, desc, link}) {
    const projects = document.createElement('div');
    projects.classList.add('work');
    const p = `
      <a class="text-decoration-none text--main project-card" href="${link}" target="_blank">
        <div class="image-container app-image-container">
          <div style="background-image: url('${imgSource(
            img
          )}')" class="img"></div>
          <div class="kind text--white">${category}</div>
        </div>
        <div class="work-text">
          <h3 class="title">${title}</h3>
          <p class="desc">${desc}</p>
        </div>
      </a>
      `;
    projects.innerHTML = p;
    return projects;
  }
  function createDesignContainer({title, img, link}) {
    const design = document.createElement('div');
    design.classList.add('work');
    const p = `
      <a class="text-decoration-none text--main project-card" href="${link}" target="_blank">
        <div class="image-container design-image-container">
          <div style="background-image: url('${imgSource(
            img
          )}')" class="img"></div>
        </div>
        <div class="work-text">
          <h3 class="title">${title}</h3>
        </div>
      </a>
      `;
    design.innerHTML = p;
    return design;
  }
  const projectsSection = document.querySelector('#projects');
  function generateProjectsBlock() {
    projects((projs) => {
      projectsSection.innerHTML = '';
      generateDesignsBlock();
      projs['results']
        .slice()
        .reverse()
        .forEach((proj) => {
          const projectContainter = createProjectContainer(proj);
          projectsSection === null || projectsSection === void 0
            ? void 0
            : projectsSection.appendChild(projectContainter);
        });
    });
  }
  function generateDesignsBlock() {
    designs((ds) => {
      ds['results']
        .slice()
        .reverse()
        .forEach((design) => {
          const designContainter = createDesignContainer(design);
          projectsSection === null || projectsSection === void 0
            ? void 0
            : projectsSection.appendChild(designContainter);
        });
    });
  }
  generateProjectsBlock();
}
export default project;
