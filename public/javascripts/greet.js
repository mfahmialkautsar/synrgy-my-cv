import {typewrite, stopTypewrite} from './typewrite.js';

function greet() {
  const greetOuter = $('.greet-outer');
  const greet1 = $('.greet-1');
  const greet1Inner = $('.greet-1-inner');
  const greet2 = $('.greet-2');
  greet1.addClass('greet-1--animate');
  greet1Inner.addClass('greet-1-inner--animate');
  greet2.addClass('greet-2--animate');
  greetOuter.removeClass('d-none');
  greet1.on('animationstart', () => {
    greetOuter.next().addClass('d-none')
    stopTypewrite();
  });
  greet2.on('animationend', () => {
    typewrite();
    greetOuter.next().removeClass('d-none')
    greetOuter.parent().children().length == 1 &&
    greetOuter.after('<div class="pt-3"><a style="color: var(--main-3); font-size: 1.3rem; opacity: .6; font-weight: 500;" href="/blog">Blog &#8250;</a></div>')
  });
}
export default greet;
