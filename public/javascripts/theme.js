const fetchNightTheme = fetch('/stylesheets/night.css')
  .then((res) => res.body)
  .then((rb) => {
    if (!rb) return;
    const reader = rb.getReader();
    return new ReadableStream({
      start(controller) {
        function push() {
          reader.read().then(({done, value}) => {
            if (done) {
              controller.close();
              return;
            }
            controller.enqueue(value);
            push();
          });
        }
        push();
      },
    });
  })
  .then((stream) => {
    return new Response(stream, {
      headers: {'Content-Type': 'text/css'},
    }).text();
  });
const setNightTheme = (completion) =>
  fetchNightTheme.then(completion).catch(() => {
    setNightTheme(completion);
  });
function theme() {
  let theme = localStorage.getItem('theme');
  const themeIcon = $('.theme-switcher i');
  if (!themeIcon) return;
  if (theme != 'night') {
    theme = 'day';
    themeIcon.addClass('fa-sun-o');
  } else {
    theme = 'night';
    themeIcon.addClass('fa-moon-o');
  }
  setTheme(theme);
  const themeButton = $('.theme-switcher');
  if (!themeButton) return;
  themeButton.attr('data-mode', theme);
  let rotate = true;
  themeButton.click(function () {
    if (!themeButton || !themeIcon) return;
    const mode = themeButton.attr('data-mode');
    if (mode == 'day') {
      themeButton.attr('data-mode', 'night');
      themeIcon.removeClass('fa-sun-o');
      themeIcon.addClass('fa-moon-o');
    } else if (mode == 'night') {
      themeButton.attr('data-mode', 'day');
      themeIcon.removeClass('fa-moon-o');
      themeIcon.addClass('fa-sun-o');
    }
    if (rotate) {
      themeIcon.addClass('rotate');
    } else {
      themeIcon.removeClass('rotate');
    }
    rotate = !rotate;
    setTheme(this.dataset.mode);
  });
  function setTheme(mode) {
    if (!mode) return;
    const themeStyle = document.getElementById('theme-style');
    if (!themeStyle) {
      const themeStyle = document.createElement('style');
      themeStyle.id = 'theme-style';
      $('head').append(themeStyle);
      setTheme(mode);
      return;
    }
    function insertStyle(value) {
      if (mode == 'day') {
        themeStyle.innerHTML = '';
      } else if (mode == 'night') {
        themeStyle.innerHTML = value;
      }
    }
    setNightTheme(insertStyle);
    localStorage.setItem('theme', mode);
  }
}
export default theme;
