import theme from './theme.js';
import animate from './animate.js';
import project from './project.js';

$(document).ready(() => {
  const themeSwitcher = `
  <div class="theme-switcher">
    <i class="fa fa-2x" title="Theme Switcher" aria-label="Theme Switcher"></i>
  </div>`;

  switch (location.pathname) {
    case '/':
      $('#section-main').replaceWith(`
      <header id="header" class="d-none d-sm-flex flex-column justify-content-center">
        <nav id="navbar" class="navbar">
          <ul class="nav navbar nav-menu">
            <li class="nav-item"><a href="#home" class="nav-link active"><i class="fa fa-home m-auto"></i> <span>Home</span></a></li>
            <li class="nav-item"><a href="#portfolio" class="nav-link"><i class="fa fa-file-o m-auto"></i> <span>Portfolio</span></a></li>
            <li class="nav-item"><a href="#contact-me" class="nav-link"><i class="fa fa-envelope m-auto"></i> <span>Contact</span></a></li>
          </ul>
        </nav>
      </header>
      ${themeSwitcher}
      <main id="home">
        <section class="greeting-container container d-flex flex-column justify-content-center align-items-start">
          <h1 class="greet-outer d-none">
            <div class="greet-1">
              <div style="opacity: 0;">AAL</div>
              <div class="greet-1-inner">Hi!</div>
            </div>
            <div class="greet-2">I'm <span class="text-highlighted">Fahmi</span>.</div>
            <span class="typewrite" data-texts='["Junior Software Developer."]'><span class="wrap"></span></span>
          </h1>
        </section>
      </main>
      <section id="portfolio" class="container projects" id="projects-projects">
        <h2>Portfolio</h2>
        <div class="projects-item" id="projects">
          <div class="work">
            <a class="text-decoration-none text--main project-card" target="_blank">
              <div class="image-container app-image-container">
                <div style="background-color: lightgrey" class="img"></div>
                <div class="kind text--white">&nbsp;</div>
              </div>
              <div class="work-text">
                <h3 class="title">&nbsp;</h3>
                <p class="desc">&nbsp;</p>
              </div>
            </a>
          </div>
          <div class="work">
            <a class="text-decoration-none text--main project-card" target="_blank">
              <div class="image-container app-image-container">
                <div style="background-color: lightgrey" class="img"></div>
                <div class="kind text--white">&nbsp;</div>
              </div>
              <div class="work-text">
                <h3 class="title">&nbsp;</h3>
                <p class="desc">&nbsp;</p>
              </div>
            </a>
          </div>
          <div class="work">
            <a class="text-decoration-none text--main project-card" target="_blank">
              <div class="image-container app-image-container">
                <div style="background-color: lightgrey" class="img"></div>
                <div class="kind text--white">&nbsp;</div>
              </div>
              <div class="work-text">
                <h3 class="title">&nbsp;</h3>
                <p class="desc">&nbsp;</p>
              </div>
            </a>
          </div>
          <div class="work">
            <a class="text-decoration-none text--main project-card" target="_blank">
              <div class="image-container app-image-container">
                <div style="background-color: lightgrey" class="img"></div>
                <div class="kind text--white">&nbsp;</div>
              </div>
              <div class="work-text">
                <h3 class="title">&nbsp;</h3>
                <p class="desc">&nbsp;</p>
              </div>
            </a>
          </div>
          <div class="work">
            <a class="text-decoration-none text--main project-card" target="_blank">
              <div class="image-container app-image-container">
                <div style="background-color: lightgrey" class="img"></div>
                <div class="kind text--white">&nbsp;</div>
              </div>
              <div class="work-text">
                <h3 class="title">&nbsp;</h3>
                <p class="desc">&nbsp;</p>
              </div>
            </a>
          </div>
        </div>
      </section>
      <section id="contact-me" class="container p-4 pb-5">
        <h2>Send me an Email</h2>
        <div>
          <form name="contact" id="form-contact" class="container">
            <input type="text" id="name" name="name" placeholder="Name" class="p-1 px-2" required><br>
            <textarea type="text" id="message" name="message" placeholder="Message" class="mt-3 p-1 px-2" required></textarea>
            <button type="submit" id="submit" class="btn mt-3 btn-main">Send</button>
          </form>
        </div>
      </section>
    `);
      project();
      animate();
      $("form[name='contact']").validate({
        rules: {
          name: 'required',
          message: 'required',
        },
        messages: {
          name: "Hi! I don't know you. You can tell me your name",
          message: 'What message do you wanna send to me?',
        },
        submitHandler: function (form) {
          const name = encodeURIComponent('Email from ' + form[0].value);
          const message = encodeURIComponent(form[1].value);
          window.location = `mailto:mfahmialkautsar@man4-jkt.sch.id?subject=${name}&body=${message}`;
        },
      });
      break;
    default:
      break;
  }

  theme();
});
