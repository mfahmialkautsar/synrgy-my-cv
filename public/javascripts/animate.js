import greet from './greet.js';
function animate() {
  greet();

  $(document).on('click', 'a[href^="#"]', function (e) {
    const id = $(this).attr('href');
    const $id = $(id);
    if ($id.length === 0) {
      return;
    }
    e.preventDefault();
    const pos = $id.offset().top;
    $('body, html').animate({scrollTop: pos});
  });
}
export default animate;
